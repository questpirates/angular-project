import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '../../../node_modules/@angular/router';
import { AuthService } from '../auth.service';

const MINUTES_UNITL_AUTO_LOGOUT = 25 // in mins
const CHECK_INTERVAL = 1000 // in ms

@Injectable({
  providedIn: 'root'
})
export class DataserviceService {
  baseApiUrl: string = "";
  isLoading: boolean;
  onlineShoppingBaseUrl: string = "";
  IsLoggedIn: boolean = false;
  SignInData: any;
  Userdetails: any;
  lastAction: any;
  constructor(private http: HttpClient,
    private router: Router, private authService: AuthService
  ) {
    // this.check();
    // this.initListener();
    // this.initInterval();
    this.onlineShoppingBaseUrl = 'http://localhost:50084/';
    this.SignInData = {};
  }

  initListener() {
    document.body.addEventListener('click', () => this.reset());
  }

  reset() {
    this.lastAction = Date.now();
  }

  initInterval() {
    setInterval(() => {
      this.check();
    }, CHECK_INTERVAL);
  }

  check() {
    debugger;
    const now = Date.now();
    const timeleft = this.lastAction + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;
    if (isTimeout) {
      localStorage.removeItem('currentUser');
      this.router.navigate(['']);
    }
  }

  get(relativeUrl: string) {
    this.isLoading = true;
    let url = this.onlineShoppingBaseUrl + relativeUrl;
    return this.http.get(url, { headers: this.getHeaderOptions() });
  }
  getgrid(relativeUrl: string, param: HttpParams) {
    this.isLoading = true;
    let url = this.onlineShoppingBaseUrl + relativeUrl;
    return this.http.get(url, { params: param });
  }
  post(relativeUrl: string, resource: any) {
    debugger;
    this.isLoading = true;
    let url = this.onlineShoppingBaseUrl + relativeUrl;
    return this.http.post(url, resource, { headers: this.getHeaderOptions() });
  }
  signupPost(relativeUrl: string, resource: any) {
    debugger;
    this.isLoading = true;
    let url = this.onlineShoppingBaseUrl + relativeUrl;
    return this.http.post(url, resource);
  }
  delete(relativeUrl: string) {
    this.isLoading = true;
    let url = this.onlineShoppingBaseUrl + relativeUrl;
    return this.http.delete(url, { headers: this.getHeaderOptions() });
  }
  upload(relativeUrl, data: FormData) {
    this.isLoading = true;
    // var headers = new HttpHeaders();
    // headers.set('Content-Type', 'multipart/form-data');
    // headers.set('Content-Type', 'application/json');
    // return this.http.post(this.onlineShoppingBaseUrl + relativeUrl, data, { headers: headers });//.pipe(
    return this.http.post(this.onlineShoppingBaseUrl + relativeUrl, data, { headers: this.getHeaderOptions() });
  }

  put(relativeUrl: string, resource: any) {
    this.isLoading = true;
    let url = this.onlineShoppingBaseUrl + relativeUrl;
    return this.http.put(url, resource, { headers: this.getHeaderOptions() })
  }

  signIn(credentials: any) {
    this.SignInData.UserEmail = credentials.UserEmail;
    this.SignInData.Password = credentials.Password;
    var headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    return this.http.post(this.onlineShoppingBaseUrl + "authorization/login", this.SignInData, { headers: headers });
  }

  getHeaderOptions() {
    debugger;
    this.Userdetails = JSON.parse(localStorage.getItem('currentUser'));
    if ((this.Userdetails == undefined || this.Userdetails == null)) {
      this.router.navigate(['login/']);
    }
    else {
      const authToken = 'Bearer ' + this.Userdetails.Token;
      const headers = new HttpHeaders().set('Authorization', authToken);
      return headers;
    }
  }
}
