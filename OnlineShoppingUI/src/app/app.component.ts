import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '../../node_modules/@angular/router';
import { AuthService } from './auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AuthService]
})
export class AppComponent implements OnInit, OnDestroy {

  UserName: string;
  isLoggedIn: boolean = false;
  constructor(private router: Router,
    private authservice: AuthService) {
    this.checkLogin();
  }

  ngOnInit() {

  }

  checkLogin() {
    this.isLoggedIn = JSON.parse(localStorage.getItem('currentUser')) ? true : false;
    if (this.isLoggedIn) {
      let UserDetail = JSON.parse(localStorage.getItem('currentUser'));
      this.UserName = UserDetail.UserName;
    }
    else {
      this.UserName = '';
    }
  }

  ngOnDestroy() {
    localStorage.removeItem('currentUser');
  }

  onLogout() {
    localStorage.removeItem('currentUser');
    this.authservice.toggleLoggedId(false);
    this.checkLogin();
    //Swal('Success','You have successfully logged out','success');
    Swal({
      position: 'center',
      type: 'success',
      title: 'You have successfully logged out',
      showConfirmButton: false,
      timer: 1000
    });
    this.router.navigate(['home/']);
  }
}
