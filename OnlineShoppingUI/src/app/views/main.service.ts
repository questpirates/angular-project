import { Injectable } from '@angular/core';
import { DataserviceService } from '../shared/dataservice.service';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private dataservice: DataserviceService) { }

  getUserProfileData(id: number)
  {
    return this.dataservice.get('users/getsignupdata/'+id);
  }
}
