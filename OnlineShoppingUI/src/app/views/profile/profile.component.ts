import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [MainService]
})
export class ProfileComponent implements OnInit {

  UserData: any;
  Uid: number;
  constructor(private mainService: MainService
    ) { 
    this.UserData = {};
    this.Uid = JSON.parse(localStorage.getItem('currentUser')).UserId;
    this.GetData();
  }

  ngOnInit() {
  }

  GetData()
  {
    this.mainService.getUserProfileData(this.Uid).subscribe(
      data => {
        this.UserData = data;
      },
      err => {
        swal('Error!','Something went wrong!','error');
      }
    );
  }

}
