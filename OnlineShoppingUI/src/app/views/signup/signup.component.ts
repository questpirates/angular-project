import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { DataserviceService } from '../../shared/dataservice.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupData: any;
  response: any;
  constructor(private router: Router,
    private dataService: DataserviceService) {
    this.signupData = {};
    this.signupData.Gender = 'Male';
  }

  ngOnInit() {
  }

  signIn(status: any) {
    if (status == 'VALID') {
      debugger;
      this.dataService.signupPost('users/savesignupdata', this.signupData).subscribe(
        data => {
          debugger;
          this.response = data;
          if (this.response == 'success') {
            Swal('Success!', 'You have successfully signed up, now you can login', 'success');
            this.router.navigate(['login/']);
          }
          else if (this.response == 'Email exists') {
            Swal('Warning!', 'Email Id is already registered', 'warning');
          }
        },
        err => {
          Swal('Oops...', 'Something went wrong', 'error');
        }
      );
    }
    else {
      Swal('Warning!', 'All fields are required', 'warning');
    }
  }
}
