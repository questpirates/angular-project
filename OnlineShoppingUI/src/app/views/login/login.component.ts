import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataserviceService } from '../../shared/dataservice.service';
import { AuthService } from '../../auth.service';
import { AppComponent } from '../../app.component'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  LoginData: any;
  loginError: any;
  Userdetails: any;
  constructor(private router: Router,
    private dataService: DataserviceService,
    private AuthService: AuthService,
    private appcomponent: AppComponent) {
    this.LoginData = {};
  }

  ngOnInit() {
  }

  login(status: any) {
    if(status == 'VALID'){
      this.dataService.signIn(this.LoginData).subscribe(
        data => {
          //Swal('Success!', 'You have successfully logged in', 'success');
          Swal({
            position: 'center',
            type: 'success',
            title: 'You have successfully logged in',
            showConfirmButton: false,
            timer: 1000
          });
          this.checkData(data);
        },
        err => {
          Swal('Error!', 'Email/Password is incorrect', 'error');
        }
      );
    }
    else{
      Swal('Warning!','Both fields are required','warning');
    }
  }

  checkData(data: any) {
    if (data.LoginStatus) {
      localStorage.setItem('currentUser', JSON.stringify({ Token: data.Token, UserName: data.UserName, UserId: data.UserId }));
      debugger;
      if (data.IsFirstTimeLogin === false) {
        this.router.navigate(['register/']);
      } else {
        this.AuthService.toggleLoggedId(true);
        this.appcomponent.checkLogin();
        this.router.navigate(['']);
      }
    } else {
      this.loginError = data.Error;
    }
  }

}
