import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { LoginComponent } from './views/login/login.component';
import { SignupComponent } from './views/signup/signup.component';
import { ForgotPwdComponent } from './views/forgot-pwd/forgot-pwd.component';
import { AuthService } from './auth.service';
import { DataserviceService } from './shared/dataservice.service';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { MainModule } from './views/main.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    ForgotPwdComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    NgbModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    MainModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [DataserviceService,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
