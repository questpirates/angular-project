﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineShoppingAPI.Models;
using OnlineShoppingAPI.Services.Users;
using OnlineShoppingAPI.Services.Users.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineShoppingAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Route("users")]
    public class UsersController : ControllerBase
    {
        private UsersService service = new UsersService();

        #region Signup data

        [AllowAnonymous]
        [HttpPost(), Route("savesignupdata")]
        public string SaveSignupData([FromBody]UserResource data)
        {
            return service.saveUserData(data);
        }

        [AllowAnonymous]
        [HttpGet(), Route("getsignupdata/{id:long}")]
        public UserResource GetSignupData(long id)
        {
            return service.getUserData(id);
        }

        [HttpPut(), Route("updatesignupdata/{id:long}")]
        public void UpdateSignupData(long id, [FromBody]UserResource data)
        {
            service.updateUserData(id, data);
        }

        [HttpGet(), Route("getalluserdata")]
        public List<TblSignup> GetAllUsersData()
        {
            return service.getAllUserData();
        }

        [HttpDelete(), Route("deleteuser")]
        public void DeleteUser(long id)
        {
            service.deleteUser(id);
        }

        #endregion


        #region Address

        [HttpPost(), Route("saveaddress")]
        public void SaveAddress([FromBody]TblAddress data)
        {
            service.saveAddress(data);
        }

        [HttpGet(), Route("getaddress/{id:long}")]
        public TblAddress GetAddress(long id)
        {
            return service.getAddress(id);
        }

        [HttpPut(), Route("updateaddress/{id:long}")]
        public void UpdateAddress(long id, [FromBody]TblAddress data)
        {
            service.updateAddress(id, data);
        }

        #endregion
    }
}
