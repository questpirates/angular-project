﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineShoppingAPI.Models;
using OnlineShoppingAPI.Services.Products;
using OnlineShoppingAPI.Services.Products.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineShoppingAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Route("products")]
    public class ProductsController : ControllerBase
    {
        private ProductsService service = new ProductsService();

        #region Product

        [HttpPost(), Route("saveproduct")]
        public void AddProduct([FromBody]ProductResource data)
        {
            service.saveProduct(data);
        }

        [HttpGet(), Route("getproduct/{id:long}")]
        public ProductResource GetProduct(long id)
        {
            return service.getProduct(id);
        }

        [HttpPut(), Route("updateproduct/{id:long}")]
        public void UpdateProduct(long id, [FromBody]ProductResource data)
        {
            service.updateProduct(id, data);
        }

        [HttpGet(), Route("getallproducts")]
        public List<ProductResource> GetAllProducts()
        {
            return service.getAllProducts();
        }

        [HttpDelete(), Route("deleteproduct/{id:long}")]
        public void DeleteProduct(long id)
        {
            service.deleteProduct(id);
        }

        #endregion


        #region Cart and Order

        [HttpPost(), Route("addtocart")]
        public void AddToCart([FromBody]TblCart data)
        {
            service.addToCart(data);
        }

        [HttpGet(), Route("getallcartproducts/{id:long}")]
        public List<ProductResource> GetCartProducts(long id)
        {
            return service.getCartProducts(id);
        }

        [HttpPut(), Route("updatecart/{id:long}")]
        public void UpdateCart(long id, [FromBody]TblCart data)
        {
            service.updateCart(id, data);
        }

        [HttpDelete(), Route("deletecart/{id:long}")]
        public void DeleteCart(long id)
        {
            service.deleteCartProduct(id);
        }

        [HttpPost(), Route("placeorder/{id:long}")]
        public void SaveOrder(long id, [FromBody]List<ProductResource> data)
        {
            service.placeOrder(id, data);
        }

        #endregion
    }
}
