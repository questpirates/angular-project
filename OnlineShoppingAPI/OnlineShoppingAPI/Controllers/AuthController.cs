﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineShoppingAPI.Models;
using System.IdentityModel.Tokens.Jwt;
using OnlineShoppingAPI.Utility;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using OnlineShoppingAPI.Services.Users.Models;
using System.Net.Http;
using System.Net;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineShoppingAPI.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("authorization")]
    public class AuthController : ControllerBase
    {
        private OnlineShoppingContext dbcontext = new OnlineShoppingContext();

        [HttpPost(), Route("login")]
        public IActionResult Authenticate([FromBody]LoginResource data)
        {
            var user = dbcontext.TblLogin.SingleOrDefault(x => x.UserEmail == data.UserEmail);
            if (user != null)
            {
                if (user.Password == PwdHasher.CheckHashedPwd(data.Password, user.Salt))
                {
                    var UserDetails = dbcontext.TblSignup.Where(x => x.Id == user.UserId).Select(u => 
                                        new { u.Id, u.FirstName, u.MiddleName, u.LastName }).SingleOrDefault();
                    
                    // authentication successful so generate jwt token
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes("questpirates@8277788812andbmchgroups");
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                            new Claim(ClaimTypes.Name, user.Id.ToString())
                        }),
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    var tokenString = tokenHandler.WriteToken(token);
                    // return basic user info (without password) and token to store client side
                    string mName = UserDetails.MiddleName != null ? " " + UserDetails.MiddleName : "";
                    string lName = UserDetails.LastName != null ? " " + UserDetails.LastName : "";
                    return Ok(new
                    {
                        UserId = UserDetails.Id,
                        UserName = UserDetails.FirstName + mName + lName,
                        LoginStatus = true,
                        Token = tokenString
                    });
                }
                else
                {
                    return NotFound("Wrong Password");
                }
            }
            else
            {
                return NotFound("Invalid UserEmail");
            }
        }

        [HttpPost(), Route("updatepassword")]
        public HttpResponseMessage updatePassword([FromBody]UpdatePassword UpdatePasswordObj)
        {
            try
            {
                if (UpdatePasswordObj.Password.Trim() == UpdatePasswordObj.ConfirmPassword.Trim())
                {
                    var ExitData = dbcontext.TblLogin.Where(x => x.UserId == UpdatePasswordObj.UserId).Select(u => u).SingleOrDefault();
                    if (ExitData != null)
                    {
                        string salt = "";
                        //ExitData.FirstLogin = true;
                        ExitData.Password = PwdHasher.GetHashedPwd(UpdatePasswordObj.Password, out salt);
                        ExitData.Salt = salt;
                        dbcontext.Entry(ExitData).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        dbcontext.SaveChanges();
                        return new HttpResponseMessage(HttpStatusCode.OK);
                    }
                    else
                    {
                        return new HttpResponseMessage(HttpStatusCode.NotFound);
                    }
                }
                return new HttpResponseMessage(HttpStatusCode.NotAcceptable);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.NotImplemented);
            }
        }
    }
}
