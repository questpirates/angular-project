﻿using System;
using System.Collections.Generic;

namespace OnlineShoppingAPI.Models
{
    public partial class TblProduct
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long ProductTypeId { get; set; }
        public string Description { get; set; }
        public long? InStock { get; set; }
        public byte[] Image { get; set; }
        public decimal Price { get; set; }
    }
}
