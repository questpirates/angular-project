﻿using System;
using System.Collections.Generic;

namespace OnlineShoppingAPI.Models
{
    public partial class TblLogin
    {
        public long Id { get; set; }
        public string UserEmail { get; set; }
        public string Password { get; set; }
        public long UserTypeId { get; set; }
        public long UserId { get; set; }
        public string Salt { get; set; }
    }
}
