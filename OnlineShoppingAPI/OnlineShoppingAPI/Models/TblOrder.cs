﻿using System;
using System.Collections.Generic;

namespace OnlineShoppingAPI.Models
{
    public partial class TblOrder
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public long UserId { get; set; }
        public DateTime? OrderDate { get; set; }
        public long? Quantity { get; set; }
        public decimal? TotalAmount { get; set; }
    }
}
