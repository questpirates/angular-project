﻿using System;
using System.Collections.Generic;

namespace OnlineShoppingAPI.Models
{
    public partial class TblAddress
    {
        public long Id { get; set; }
        public string Building { get; set; }
        public string Area { get; set; }
        public string Landmark { get; set; }
        public string City { get; set; }
        public long PinCode { get; set; }
        public string State { get; set; }
        public long UserId { get; set; }
    }
}
