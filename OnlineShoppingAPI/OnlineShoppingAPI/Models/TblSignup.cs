﻿using System;
using System.Collections.Generic;

namespace OnlineShoppingAPI.Models
{
    public partial class TblSignup
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? Dob { get; set; }
        public string Gender { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
    }
}
