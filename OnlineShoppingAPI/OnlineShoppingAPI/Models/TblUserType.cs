﻿using System;
using System.Collections.Generic;

namespace OnlineShoppingAPI.Models
{
    public partial class TblUserType
    {
        public long Id { get; set; }
        public string Type { get; set; }
    }
}
