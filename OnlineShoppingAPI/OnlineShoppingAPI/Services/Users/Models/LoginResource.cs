﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShoppingAPI.Services.Users.Models
{
    public class LoginResource
    {
        public long Id { get; set; }
        public string UserEmail { get; set; }
        public string Password { get; set; }
        public long UserId { get; set; }
        public string Salt { get; set; }
        public string Token { get; set; }
    }
}
