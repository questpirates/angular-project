﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShoppingAPI.Services.Users.Models
{
    public class UpdatePassword
    {
        public long UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public bool FirstLogin { get; set; }
    }
}
