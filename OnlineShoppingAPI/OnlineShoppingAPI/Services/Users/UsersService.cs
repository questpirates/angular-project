﻿using OnlineShoppingAPI.Models;
using OnlineShoppingAPI.Services.Users.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineShoppingAPI.Utility;

namespace OnlineShoppingAPI.Services.Users
{
    public class UsersService
    {
        private OnlineShoppingContext dbcontext = new OnlineShoppingContext();

        #region Signup data

        public string saveUserData(UserResource data)
        {
            TblSignup user = dbcontext.TblSignup.FirstOrDefault(x => x.EmailId == data.EmailId);
            if(user != null)
            {
                return "Email exists";
            }
            else
            {
                string salt = "";
                TblSignup signupObj = new TblSignup()
                {
                    FirstName = data.FirstName,
                    MiddleName = data.MiddleName,
                    LastName = data.LastName,
                    Dob = data.Dob,
                    Gender = data.Gender,
                    EmailId = data.EmailId,
                    MobileNo = data.MobileNo,
                };
                dbcontext.TblSignup.Add(signupObj);
                dbcontext.SaveChanges();

                TblLogin loginObj = new TblLogin()
                {
                    UserEmail = data.EmailId,
                    Password = PwdHasher.GetHashedPwd(data.Password, out salt),
                    Salt = salt,
                    UserId = signupObj.Id,
                    UserTypeId = 1
                };
                dbcontext.TblLogin.Add(loginObj);
                dbcontext.SaveChanges();
                return "success";
            }
        }

        public UserResource getUserData(long id)
        {
            UserResource userObj = (from tblsignup in dbcontext.TblSignup
                                    join tbllogin in dbcontext.TblLogin on tblsignup.Id equals tbllogin.UserId
                                    where tblsignup.Id == id
                                    select new UserResource()
                                    {
                                        FirstName = tblsignup.FirstName,
                                        MiddleName = tblsignup.MiddleName,
                                        LastName = tblsignup.LastName,
                                        Dob = tblsignup.Dob,
                                        EmailId = tblsignup.EmailId,
                                        Gender = tblsignup.Gender,
                                        MobileNo = tblsignup.MobileNo
                                    }).FirstOrDefault();
            return userObj;
        }

        public void updateUserData(long id, UserResource data)
        {
            TblSignup signupObj = dbcontext.TblSignup.FirstOrDefault(x => x.Id == id);
            signupObj.FirstName = data.FirstName;
            signupObj.MiddleName = data.MiddleName;
            signupObj.LastName = data.LastName;
            signupObj.Dob = data.Dob;
            signupObj.Gender = data.Gender;
            signupObj.EmailId = data.EmailId;
            signupObj.MobileNo = data.MobileNo;

            TblLogin loginObj = dbcontext.TblLogin.FirstOrDefault(x => x.UserId == id);
            loginObj.UserEmail = data.EmailId;
            dbcontext.SaveChanges();
        }

        public List<TblSignup> getAllUserData()
        {
            List<TblSignup> allUsers = dbcontext.TblSignup.OrderBy(x => x.FirstName).ToList();
            return allUsers;
        }

        public void deleteUser(long id)
        {
            TblAddress address = dbcontext.TblAddress.FirstOrDefault(x => x.UserId == id);
            if(address != null)
            {
                dbcontext.TblAddress.Remove(address);
            }

            TblLogin logindata = dbcontext.TblLogin.FirstOrDefault(x => x.UserId == id);
            dbcontext.TblLogin.Remove(logindata);

            TblSignup signupdata = dbcontext.TblSignup.FirstOrDefault(x => x.Id == id);
            dbcontext.TblSignup.Remove(signupdata);
            dbcontext.SaveChanges();
        }

        #endregion


        #region Address

        public void saveAddress(TblAddress data)
        {
            TblAddress addressObj = new TblAddress()
            {
                Building = data.Building,
                Area = data.Area,
                Landmark = data.Landmark,
                City = data.City,
                PinCode = data.PinCode,
                State = data.State,
                UserId = data.UserId
            };
            dbcontext.TblAddress.Add(addressObj);
            dbcontext.SaveChanges();
        }

        public TblAddress getAddress(long userid)
        {
            TblAddress address = dbcontext.TblAddress.FirstOrDefault(x => x.UserId == userid);
            return address;
        }

        public void updateAddress(long userid, TblAddress data)
        {
            TblAddress addressObj = dbcontext.TblAddress.FirstOrDefault(x => x.UserId == userid);
            addressObj.Building = data.Building;
            addressObj.Area = data.Area;
            addressObj.City = data.City;
            addressObj.Landmark = data.Landmark;
            addressObj.PinCode = data.PinCode;
            addressObj.State = data.State;
            dbcontext.SaveChanges();
        }

        #endregion
    }
}
