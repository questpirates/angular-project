﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShoppingAPI.Services.Products.Models
{
    public class ProductResource
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long ProductTypeId { get; set; }
        public string Description { get; set; }
        public long? InStock { get; set; }
        public string ImageStream { get; set; }
        public decimal Price { get; set; }
        public long CartId { get; set; }
        public long? Quantity { get; set; }
    }
}
