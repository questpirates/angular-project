﻿using OnlineShoppingAPI.Models;
using OnlineShoppingAPI.Services.Products.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShoppingAPI.Services.Products
{
    public class ProductsService
    {
        private OnlineShoppingContext dbcontext = new OnlineShoppingContext();

        #region Product

        public void saveProduct(ProductResource product)
        {
            TblProduct productObj = new TblProduct()
            {
                Name = product.Name,
                Description = product.Description,
                ProductTypeId = product.ProductTypeId,
                InStock = product.InStock,
                Price = product.Price
            };
            if (product.ImageStream != null)
            {
                string base64String = product.ImageStream.Substring(product.ImageStream.IndexOf(',') + 1);
                byte[] image = Convert.FromBase64String(base64String);
                productObj.Image = image;
            }
            dbcontext.TblProduct.Add(productObj);
            dbcontext.SaveChanges();
        }

        public ProductResource getProduct(long id)
        {
            ProductResource product = dbcontext.TblProduct.Where(x => x.Id == id).Select(x => new ProductResource()
            {
                Name = x.Name,
                Description = x.Description,
                InStock = x.InStock,
                Price = x.Price,
                ImageStream = Convert.ToBase64String(x.Image)
            }).FirstOrDefault();
            return product;
        }

        public void updateProduct(long id, ProductResource product)
        {
            TblProduct productObj = dbcontext.TblProduct.FirstOrDefault(x => x.Id == id);
            productObj.Name = product.Name;
            productObj.Description = product.Description;
            productObj.InStock = product.InStock;
            productObj.Price = product.Price;
            if (product.ImageStream != null)
            {
                string base64String = product.ImageStream.Substring(product.ImageStream.IndexOf(',') + 1);
                byte[] image = Convert.FromBase64String(base64String);
                productObj.Image = image;
            }
            dbcontext.SaveChanges();
        }

        public List<ProductResource> getAllProducts()
        {
            List<ProductResource> products = dbcontext.TblProduct.Select(x => new ProductResource()
            {
                Name = x.Name,
                Description = x.Description,
                InStock = x.InStock,
                Price = x.Price,
                ImageStream = Convert.ToBase64String(x.Image)
            }).OrderBy(x => x.Name).ToList();
            return products;
        }

        public void deleteProduct(long id)
        {
            TblProduct product = dbcontext.TblProduct.FirstOrDefault(x => x.Id == id);
            dbcontext.TblProduct.Remove(product);
            dbcontext.SaveChanges();
        }

        #endregion


        #region Cart

        public void addToCart(TblCart data)
        {
            TblCart entity = new TblCart()
            {
                ProductId = data.ProductId,
                UserId = data.UserId,
                Quantity = data.Quantity
            };
            dbcontext.TblCart.Add(entity);
            dbcontext.SaveChanges();
        }

        public void updateCart(long id, TblCart data)
        {
            TblCart entity = dbcontext.TblCart.FirstOrDefault(x => x.Id == id);
            entity.Quantity = data.Quantity;
            dbcontext.SaveChanges();
        }

        public List<ProductResource> getCartProducts(long userid)
        {
            List<ProductResource> products = (from tblproduct in dbcontext.TblProduct
                                              join tblcart in dbcontext.TblCart on tblproduct.Id equals tblcart.ProductId
                                              where tblcart.UserId == userid
                                              select new ProductResource()
                                              {
                                                  Id = tblproduct.Id,
                                                  CartId = tblcart.Id,
                                                  Name = tblproduct.Name,
                                                  Description = tblproduct.Description,
                                                  Price = tblproduct.Price,
                                                  Quantity = tblcart.Quantity,
                                                  //ImageStream =
                                              }).ToList();
            return products;
        }

        public void deleteCartProduct(long id)
        {
            TblCart entity = dbcontext.TblCart.FirstOrDefault(x => x.Id == id);
            dbcontext.TblCart.Remove(entity);
            dbcontext.SaveChanges();
        }

        #endregion


        #region Order

        public void placeOrder(long userid, List<ProductResource> products)
        {
            foreach(var item in products)
            {
                TblOrder order = new TblOrder()
                {
                    ProductId = item.Id,
                    UserId = userid,
                    Quantity = item.Quantity,
                    TotalAmount = item.Quantity * item.Price,
                    OrderDate = DateTime.Now
                };
                dbcontext.TblOrder.Add(order);
                dbcontext.SaveChanges();
            }
            dbcontext.RemoveRange(dbcontext.TblCart.Where(x => x.UserId == userid));
            dbcontext.SaveChanges();
        }

        #endregion

    }
}
